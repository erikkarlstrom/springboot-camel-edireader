package camel.edireader;

import com.berryworks.edireader.EDIReader;
import com.berryworks.edireader.EdifactReader;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

@Service
public class EdifactReaderService {

    EdifactReader edifactReader = new EdifactReader();

    public String convert(String input) throws TransformerException {
        StringReader reader = new StringReader(input);
        StringWriter writer = new StringWriter();
        ediToXml(reader, writer, edifactReader);
        return writer.toString();
    }

    public void ediToXml(Reader ediInput, Writer xmlOutput, EDIReader parser) throws TransformerException {
        InputSource inputSource = new InputSource(ediInput);
        SAXSource source = new SAXSource(parser, inputSource);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(xmlOutput);
        transformer.transform(source, result);
    }
}
