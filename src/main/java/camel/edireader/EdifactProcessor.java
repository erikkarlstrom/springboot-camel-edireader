package camel.edireader;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;


@Component
public class EdifactProcessor implements Processor {

    final private EdifactReaderService edifactReaderService;

    public EdifactProcessor(EdifactReaderService edifactReaderService){
        this.edifactReaderService = edifactReaderService;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String xml = edifactReaderService.convert(exchange.getIn().getBody(String.class));
        Message message = exchange.getMessage();
        message.setBody(xml);
        exchange.setMessage(message);
    }
}
