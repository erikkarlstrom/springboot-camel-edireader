package camel.edireader;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class EdiFileReaderRoute extends RouteBuilder {

    private final EdifactProcessor edifactProcessor;

    public EdiFileReaderRoute(EdifactProcessor edifactProcessor){
        this.edifactProcessor = edifactProcessor;
    }

    @Override
    public void configure() throws Exception {
        from("file:data-in?noop=true")
                .process(edifactProcessor)
                .setHeader("CamelFileName", simple("${header[CamelFileName].replace('.edi','.xml')}"))
                .to("file:data-out");
    }
}
